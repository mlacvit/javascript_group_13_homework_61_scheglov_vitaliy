import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { GaleryComponent } from './galery/galery.component';
import { MusicComponent } from './music/music.component';
import { VideoComponent } from './video/video.component';
import { ModalComponent } from './modal/modal.component';
import { TicketComponent } from './ticket/ticket.component';
import { ErrorPageComponent } from './error-page.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'galery', component: GaleryComponent},
  {path: 'music', component: MusicComponent},
  {path: 'video', component: VideoComponent},
  {path: 'modal', component: ModalComponent},
  {path: 'ticket', component: TicketComponent},
  {path: '**', component: ErrorPageComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
