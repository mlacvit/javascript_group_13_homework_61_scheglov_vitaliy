import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MusicComponent } from './music/music.component';
import { GaleryComponent } from './galery/galery.component';
import { MenuComponent } from './menu/menu.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { VitalServices } from './vital.services';
import { VideoComponent } from './video/video.component';
import { ModalComponent } from './modal/modal.component';
import { TicketComponent } from './ticket/ticket.component';
import { ErrorPageComponent } from './error-page.component';


@NgModule({
  declarations: [
    AppComponent,
    MusicComponent,
    GaleryComponent,
    MenuComponent,
    ContactComponent,
    HomeComponent,
    VideoComponent,
    ModalComponent,
    TicketComponent,
    ErrorPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [VitalServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
