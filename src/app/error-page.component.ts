import { Component } from '@angular/core';

@Component({
  selector: 'app-error-page',
  template:
    `<div>
        <h1>открыть в whatsApp</h1>
        <a href="https://wa.me/551590950">WHATSAPP
        </a>
    </div>`,
  styles: [`
    div{color: goldenrod; text-align: center; margin:  100px auto}
    a{color: goldenrod; text-align: center}`]
})

export class ErrorPageComponent {

}
